import { LOGIN_API_URL } from "../constants/appConstants";

export async function login(credentials) {
  try {
    const loginResponse = await fetch(LOGIN_API_URL, {
      method: "post",
      body: JSON.stringify(credentials),
      headers: {
        "Content-Type": "application/json",
      },
    });
    const parsedResponse = loginResponse.json();
    if (loginResponse.ok) {
      return parsedResponse;
    }

    return Promise.reject(parsedResponse);
  } catch (e) {
    return Promise.reject(e);
  }
}
