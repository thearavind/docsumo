import { Navigate, Routes, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import LoginScreen from "./screens/Login/Login";
import WelcomeScreen from "./screens/Welcome/Welcome";
import Layout from "./components/Layout/Layout";
import ProtectedRoute from "./components/Protected/Protected";
import { useMemo } from "react";

const Router = () => {
  const token = useSelector((state) => state.user.token);

  const ProtectedWelcomeScreen = useMemo(
    () => ProtectedRoute(WelcomeScreen, token),
    [token]
  );

  return (
    <Routes>
      <Route element={<Layout />}>
        <Route
          path="/"
          element={<Navigate to="/login" state={{ from: null }} replace />}
        />
        <Route path="/login" element={<LoginScreen />} />
        <Route path="/welcome" element={<ProtectedWelcomeScreen />} />
      </Route>
    </Routes>
  );
};

export default Router;
