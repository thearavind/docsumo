import { useSelector } from "react-redux";

import "./Welcome.css";

const Welcome = () => {
  const userDetails = useSelector((state) => state.user.userDetails);

  return <h1 className="welcome-text">Welcome {userDetails?.full_name}</h1>;
};

export default Welcome;
