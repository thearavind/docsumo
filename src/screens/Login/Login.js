import { Navigate } from "react-router-dom";
import LoginForm from "../../components/LoginForm/LoginForm";
import { useDispatch, useSelector } from "react-redux";
import { doLogin } from "../../slice/userSlice";

const Login = () => {
  const dispatch = useDispatch();
  const userError = useSelector((state) => state.user.error);
  const isLoading = useSelector((state) => state.user.loading);
  const token = useSelector((state) => state.user.token);

  const onSubmit = (values) => {
    dispatch(doLogin(values));
  };

  if (token) {
    return <Navigate to="/welcome" state={{ from: "/login" }} />;
  }

  return (
    <LoginForm
      onSubmit={onSubmit}
      isLoading={isLoading}
      userError={userError}
    />
  );
};

export default Login;
