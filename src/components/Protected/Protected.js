import { Navigate } from "react-router-dom";

function ProtectedRoute(WrappedComponent, token) {
  return function WithWrapper(props) {
    if (!token) {
      // Redirect them to the /login page
      return <Navigate to="/login" state={{ from: null }} replace />;
    }
    return <WrappedComponent {...props} />;
  };
}

export default ProtectedRoute;
