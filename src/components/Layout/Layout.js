import { Outlet } from "react-router-dom";
import Logo from "../../assets/docsumo-logo.png";

import "./Layout.css";

const Layout = () => {
  return (
    <>
      <header>
        <a href="/">
          <img className="logo" src={Logo} alt="Docsumo" />
        </a>
      </header>
      <main>
        <Outlet />
      </main>
    </>
  );
};

export default Layout;
