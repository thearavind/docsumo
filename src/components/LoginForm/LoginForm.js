import {
  Button,
  Card,
  Form,
  Input,
  Typography,
  Alert,
  Space,
  Spin,
} from "antd";
import React from "react";

import "./LoginForm.css";

const { Title, Text, Link } = Typography;

const LoginForm = ({ onSubmit, isLoading, userError }) => {
  const onFinish = (values) => {
    onSubmit(values);
  };

  return (
    <Card className="login-form">
      {isLoading ? (
        <Space className="loader-space" size="middle">
          <Spin size="large" />
        </Space>
      ) : (
        <Form
          name="basic"
          layout="vertical"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Title level={3}>Login to your Docsumo account</Title>
          {userError ? (
            <Alert className="error-alert" message={userError} type="error" />
          ) : null}
          <Form.Item
            label="Work email"
            name="email"
            rules={[{ required: true, message: "Email is required" }]}
          >
            <Input placeholder="janedoe@abc.com" />
          </Form.Item>

          <Form.Item
            className="password"
            label="Password"
            name="password"
            placeholder="Enter password here..."
            rules={[{ required: true, message: "Password is required" }]}
          >
            <Input.Password />
          </Form.Item>
          <div className="forgot">
            <Link>Forgot Password?</Link>
          </div>
          <Form.Item>
            <Button className="login-btn" type="primary" htmlType="submit">
              Login
            </Button>
          </Form.Item>
          <div className="sign-up">
            <Text>
              Don't have an account? <Link>Sign Up</Link>
            </Text>
          </div>
        </Form>
      )}
    </Card>
  );
};

export default LoginForm;
