import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { login } from "../service/loginService";

const initialState = {
  token: null,
  userDetails: null,
  loading: false,
  error: null,
};

export const doLogin = createAsyncThunk(
  "user/login",
  async (credentials, { rejectWithValue }) => {
    try {
      const loginResponse = await login(credentials);
      return loginResponse;
    } catch (err) {
      return rejectWithValue(await err);
    }
  }
);

const { reducer } = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: {
    [doLogin.fulfilled]: (state, { meta, payload }) => {
      if (meta.requestId === state.currentRequestId.requestId) {
        state.userDetails = payload?.data?.user;
        state.token = payload?.data?.token;
        state.loading = false;
        state.currentRequestId = "";
      }
    },
    [doLogin.pending]: (state, { meta }) => {
      state.currentRequestId = meta;
      state.loading = true;
    },
    [doLogin.rejected]: (state, { meta, payload }) => {
      if (meta.requestId === state.currentRequestId.requestId) {
        state.currentRequestId = meta;
        state.loading = false;
        state.userDetails = null;
        state.error = payload.error ?? payload;
      }
    },
  },
});

export default reducer;
